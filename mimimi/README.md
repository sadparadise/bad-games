# mimimi
A bad Snake clone made in Pygame.

## Particularities

* There are no edges in the game, so if you exit the screen space you may end up exploring the void until the end of time
* The arrow keys make the snake go directly to where they're pointing to. It's not a turn-left-turn-right type of snake
* You can not turn back, like going left after you go right.

## Controls

* q - exit the game
* arrow keys - control direction

It's as basic as this. Kinda trash, don't need to tell.
